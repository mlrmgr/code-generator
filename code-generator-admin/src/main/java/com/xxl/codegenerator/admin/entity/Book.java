package com.xxl.codegenerator.admin.entity;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import java.io.Serializable;

/**
 *
 *
 *  Created by xuxueli on '2019-04-25 00:32:41'.
 */
public class Book implements Serializable {
    private static final long serialVersionUID = 42L;

    /**
     * id
     */
    private int id;

    /**
     * book_id
     */
    private int bookId;

    /**
     * book_name
     */
    private String bookName;

    /**
     * book_owner
     */
    private String bookOwner;

    /**
     * book_class
     */
    private String bookClass;

    /**
     * book_status
     */
    private int bookStatus;

    /**
     * book_num
     */
    private int bookNum;

    /**
     * book_from
     */
    private String bookFrom;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookOwner() {
        return bookOwner;
    }

    public void setBookOwner(String bookOwner) {
        this.bookOwner = bookOwner;
    }

    public String getBookClass() {
        return bookClass;
    }

    public void setBookClass(String bookClass) {
        this.bookClass = bookClass;
    }

    public int getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(int bookStatus) {
        this.bookStatus = bookStatus;
    }

    public int getBookNum() {
        return bookNum;
    }

    public void setBookNum(int bookNum) {
        this.bookNum = bookNum;
    }

    public String getBookFrom() {
        return bookFrom;
    }

    public void setBookFrom(String bookFrom) {
        this.bookFrom = bookFrom;
    }

}
