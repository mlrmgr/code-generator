package com.xxl.codegenerator.admin.entity;
import java.io.Serializable;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/23/2019
 */

public class User implements Serializable {
    private static final long serialVersionUID = 42L;

    /**
     * 用户ID
     */
    private int id;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * 班级
     */
    private String userClass;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserClass() {
        return userClass;
    }

    public void setUserClass(String userClass) {
        this.userClass = userClass;
    }

}
