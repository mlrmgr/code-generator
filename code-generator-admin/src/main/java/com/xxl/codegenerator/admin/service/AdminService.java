package com.xxl.codegenerator.admin.service;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.entity.Admin;
import com.xxl.codegenerator.admin.model.ReturnT;

import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:40:36'.
 */
public interface AdminService {

    /**
     * 新增
     */
    public ReturnT<String> insert(Admin admin);

    /**
     * 删除
     */
    public ReturnT<String> delete(int id);

    /**
     * 更新
     */
    public ReturnT<String> update(Admin admin);

    /**
     * Load查询
     */
    public Admin load(int id);

    /**
     * 分页查询
     */
    public Map<String,Object> pageList(int offset, int pagesize);

}

