package com.xxl.codegenerator.admin.dao;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.entity.Admin;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:40:36'.
 */
@Component
public interface AdminDao {

    /**
     * 新增
     */
    public int insert(@Param("admin") Admin admin);

    /**
     * 删除
     */
    public int delete(@Param("id") int id);

    /**
     * 更新
     */
    public int update(@Param("admin") Admin admin);

    /**
     * Load查询
     */
    public Admin load(@Param("id") int id);

    /**
     * 分页查询Data
     */
    public List<Admin> pageList(@Param("offset") int offset,
                                @Param("pagesize") int pagesize);

    /**
     * 分页查询Count
     */
    public int pageListCount(@Param("offset") int offset,
                             @Param("pagesize") int pagesize);

}

