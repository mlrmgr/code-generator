package com.xxl.codegenerator.admin.service;

import com.xxl.codegenerator.admin.entity.User;
import com.xxl.codegenerator.admin.model.ReturnT;

import java.util.Map;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/23/2019
 */
public interface UserService {

    /**
     * 新增
     */
    public ReturnT<String> insert(User user);

    /**
     * 删除
     */
    public ReturnT<String> delete(int id);

    /**
     * 更新
     */
    public ReturnT<String> update(User user);

    /**
     * Load查询
     */
    public User load(int id);

    /**
     * 分页查询
     */
    public Map<String,Object> pageList(int offset, int pagesize);

}
