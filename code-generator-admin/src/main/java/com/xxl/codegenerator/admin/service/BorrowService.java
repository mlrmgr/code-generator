package com.xxl.codegenerator.admin.service;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.entity.Borrow;
import com.xxl.codegenerator.admin.model.ReturnT;

import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:46:09'.
 */
public interface BorrowService {

    /**
     * 新增
     */
    public ReturnT<String> insert(Borrow borrow);

    /**
     * 删除
     */
    public ReturnT<String> delete(int id);

    /**
     * 更新
     */
    public ReturnT<String> update(Borrow borrow);

    /**
     * Load查询
     */
    public Borrow load(int id);

    /**
     * 分页查询
     */
    public Map<String,Object> pageList(int offset, int pagesize);

}

