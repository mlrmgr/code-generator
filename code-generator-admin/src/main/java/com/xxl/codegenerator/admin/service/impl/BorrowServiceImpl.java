package com.xxl.codegenerator.admin.service.impl;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.dao.BorrowDao;
import com.xxl.codegenerator.admin.entity.Borrow;
import com.xxl.codegenerator.admin.model.ReturnT;
import com.xxl.codegenerator.admin.service.BorrowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:46:09'.
 */
@Service
public class BorrowServiceImpl implements BorrowService {

    @Resource
    private BorrowDao borrowDao;

    /**
     * 新增
     */
    @Override
    public ReturnT<String> insert(Borrow borrow) {

        // valid
        if (borrow == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "必要参数缺失");
        }

        borrowDao.insert(borrow);
        return ReturnT.SUCCESS;
    }

    /**
     * 删除
     */
    @Override
    public ReturnT<String> delete(int id) {
        int ret = borrowDao.delete(id);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * 更新
     */
    @Override
    public ReturnT<String> update(Borrow borrow) {
        int ret = borrowDao.update(borrow);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * Load查询
     */
    @Override
    public Borrow load(int id) {
        return borrowDao.load(id);
    }

    /**
     * 分页查询
     */
    @Override
    public Map<String,Object> pageList(int offset, int pagesize) {

        List<Borrow> pageList = borrowDao.pageList(offset, pagesize);
        int totalCount = borrowDao.pageListCount(offset, pagesize);

        // result
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("pageList", pageList);
        maps.put("totalCount", totalCount);

        return maps;
    }

}
