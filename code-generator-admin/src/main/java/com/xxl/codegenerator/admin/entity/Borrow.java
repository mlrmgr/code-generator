package com.xxl.codegenerator.admin.entity;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import java.io.Serializable;
import java.util.Date;

/**
 *
 *
 *  Created by xuxueli on '2019-04-25 00:46:09'.
 */
public class Borrow implements Serializable {
    private static final long serialVersionUID = 42L;

    /**
     *
     */
    private int id;

    /**
     *
     */
    private int borrowUserId;

    /**
     *
     */
    private int borrowBookId;

    /**
     *
     */
    private Date borrowTime;

    /**
     *
     */
    private int borrowStatus;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBorrowUserId() {
        return borrowUserId;
    }

    public void setBorrowUserId(int borrowUserId) {
        this.borrowUserId = borrowUserId;
    }

    public int getBorrowBookId() {
        return borrowBookId;
    }

    public void setBorrowBookId(int borrowBookId) {
        this.borrowBookId = borrowBookId;
    }

    public Date getBorrowTime() {
        return borrowTime;
    }

    public void setBorrowTime(Date borrowTime) {
        this.borrowTime = borrowTime;
    }

    public int getBorrowStatus() {
        return borrowStatus;
    }

    public void setBorrowStatus(int borrowStatus) {
        this.borrowStatus = borrowStatus;
    }

}
