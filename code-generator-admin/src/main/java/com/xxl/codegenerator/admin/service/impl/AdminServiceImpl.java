package com.xxl.codegenerator.admin.service.impl;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.dao.AdminDao;
import com.xxl.codegenerator.admin.entity.Admin;
import com.xxl.codegenerator.admin.model.ReturnT;
import com.xxl.codegenerator.admin.service.AdminService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:40:36'.
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Resource
    private AdminDao adminDao;

    /**
     * 新增
     */
    @Override
    public ReturnT<String> insert(Admin admin) {

        // valid
        if (admin == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "必要参数缺失");
        }

        adminDao.insert(admin);
        return ReturnT.SUCCESS;
    }

    /**
     * 删除
     */
    @Override
    public ReturnT<String> delete(int id) {
        int ret = adminDao.delete(id);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * 更新
     */
    @Override
    public ReturnT<String> update(Admin admin) {
        int ret = adminDao.update(admin);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * Load查询
     */
    @Override
    public Admin load(int id) {
        return adminDao.load(id);
    }

    /**
     * 分页查询
     */
    @Override
    public Map<String,Object> pageList(int offset, int pagesize) {

        List<Admin> pageList = adminDao.pageList(offset, pagesize);
        int totalCount = adminDao.pageListCount(offset, pagesize);

        // result
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("pageList", pageList);
        maps.put("totalCount", totalCount);

        return maps;
    }

}
