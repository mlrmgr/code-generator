package com.xxl.codegenerator.admin.service.impl;

import java.util.List;
import com.xxl.codegenerator.admin.dao.UserDao;
import com.xxl.codegenerator.admin.entity.User;
import com.xxl.codegenerator.admin.model.ReturnT;
import com.xxl.codegenerator.admin.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/23/2019
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    /**
     * 新增
     */
    @Override
    public ReturnT<String> insert(User user) {

        // valid
        if (user == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "必要参数缺失");
        }

        userDao.insert(user);
        return ReturnT.SUCCESS;
    }

    /**
     * 删除
     */
    @Override
    public ReturnT<String> delete(int id) {
        int ret = userDao.delete(id);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * 更新
     */
    @Override
    public ReturnT<String> update(User user) {
        int ret = userDao.update(user);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * Load查询
     */
    @Override
    public User load(int id) {
        return userDao.load(id);
    }

    /**
     * 分页查询
     */
    @Override
    public Map<String,Object> pageList(int offset, int pagesize) {

        List<User> pageList = userDao.pageList(offset, pagesize);
        int totalCount = userDao.pageListCount(offset, pagesize);

        // result
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("pageList", pageList);
        result.put("totalCount", totalCount);

        return result;
    }

}

