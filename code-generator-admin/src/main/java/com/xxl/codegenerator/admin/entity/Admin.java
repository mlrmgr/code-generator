package com.xxl.codegenerator.admin.entity;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import java.io.Serializable;

/**
 *
 *
 *  Created by xuxueli on '2019-04-25 00:40:36'.
 */
public class Admin implements Serializable {
    private static final long serialVersionUID = 42L;

    /**
     *
     */
    private int id;

    /**
     *
     */
    private int adminId;

    /**
     *
     */
    private String adminPassword;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }

}
