package com.xxl.codegenerator.admin.controller;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/23/2019
 */
import com.xxl.codegenerator.admin.entity.User;
import com.xxl.codegenerator.admin.model.ReturnT;
import com.xxl.codegenerator.admin.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 用户信息
 *
 * Created by xuxueli on '2019-04-23 23:25:40'.
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 新增
     */
    @RequestMapping("/insert")
    @ResponseBody
    public ReturnT<String> insert(@RequestBody User user){
        return userService.insert(user);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ReturnT<String> delete(@RequestParam int id){
        return userService.delete(id);
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @ResponseBody
    public ReturnT<String> update(@RequestBody User user){
        return userService.update(user);
    }

    /**
     * Load查询
     */
    @RequestMapping("/load")
    @ResponseBody
    public ReturnT<String> load(@RequestParam int id){
        return new ReturnT(userService.load(id));
    }

    /**
     * 分页查询
     */
    @RequestMapping("/pageList")
    @ResponseBody
    public Map<String, Object> pageList(@RequestParam(required = false, defaultValue = "0") int offset,
                                        @RequestParam(required = false, defaultValue = "10") int pagesize) {
        return userService.pageList(offset, pagesize);
    }

}

