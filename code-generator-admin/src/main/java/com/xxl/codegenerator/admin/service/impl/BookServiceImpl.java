package com.xxl.codegenerator.admin.service.impl;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.dao.BookDao;
import com.xxl.codegenerator.admin.entity.Book;
import com.xxl.codegenerator.admin.model.ReturnT;
import com.xxl.codegenerator.admin.service.BookService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:32:41'.
 */
@Service
public class BookServiceImpl implements BookService {

    @Resource
    private BookDao bookDao;

    /**
     * 新增
     */
    @Override
    public ReturnT<String> insert(Book book) {

        // valid
        if (book == null) {
            return new ReturnT<String>(ReturnT.FAIL_CODE, "必要参数缺失");
        }

        bookDao.insert(book);
        return ReturnT.SUCCESS;
    }

    /**
     * 删除
     */
    @Override
    public ReturnT<String> delete(int id) {
        int ret = bookDao.delete(id);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * 更新
     */
    @Override
    public ReturnT<String> update(Book book) {
        int ret = bookDao.update(book);
        return ret>0?ReturnT.SUCCESS:ReturnT.FAIL;
    }

    /**
     * Load查询
     */
    @Override
    public Book load(int id) {
        return bookDao.load(id);
    }

    /**
     * 分页查询
     */
    @Override
    public Map<String,Object> pageList(int offset, int pagesize) {

        List<Book> pageList = bookDao.pageList(offset, pagesize);
        int totalCount = bookDao.pageListCount(offset, pagesize);

        // result
        Map<String, Object> maps = new HashMap<String, Object>();
        maps.put("pageList", pageList);
        maps.put("totalCount", totalCount);

        return maps;
    }

}
