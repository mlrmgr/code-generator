package com.xxl.codegenerator.admin.controller;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.entity.Admin;
import com.xxl.codegenerator.admin.model.ReturnT;
import com.xxl.codegenerator.admin.service.AdminService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:40:36'.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Resource
    private AdminService adminService;

    /**
     * 新增
     */
    @RequestMapping("/insert")
    @ResponseBody
    public ReturnT<String> insert(@RequestBody Admin admin){
        return adminService.insert(admin);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ReturnT<String> delete(@RequestParam int id){
        return adminService.delete(id);
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @ResponseBody
    public ReturnT<String> update(@RequestBody Admin admin){
        return adminService.update(admin);
    }

    /**
     * Load查询
     */
    @RequestMapping("/load")
    @ResponseBody
    public ReturnT<String> load(@RequestParam int id){
        return new ReturnT(adminService.load(id));
    }

    /**
     * 分页查询
     */
    @RequestMapping("/pageList")
    @ResponseBody
    public Map<String, Object> pageList(@RequestParam(required = false, defaultValue = "0") int offset,
                                        @RequestParam(required = false, defaultValue = "10") int pagesize) {
        return adminService.pageList(offset, pagesize);
    }

}

