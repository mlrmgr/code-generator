package com.xxl.codegenerator.admin.controller;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.entity.Book;
import com.xxl.codegenerator.admin.model.ReturnT;
import com.xxl.codegenerator.admin.service.BookService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:32:41'.
 */
@Controller
@RequestMapping("/book")
public class BookController {

    @Resource
    private BookService bookService;

    /**
     * 新增
     */
    @RequestMapping("/insert")
    @ResponseBody
    public ReturnT<String> insert(@RequestBody Book book){
        return bookService.insert(book);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ReturnT<String> delete(@RequestParam int id){
        return bookService.delete(id);
    }

    /**
     * 更新
     */
    @RequestMapping("/update")
    @ResponseBody
    public ReturnT<String> update(@RequestBody Book book){
        return bookService.update(book);
    }

    /**
     * Load查询
     */
    @RequestMapping("/load")
    @ResponseBody
    public ReturnT<String> load(@RequestParam int id){
        return new ReturnT(bookService.load(id));
    }

    /**
     * 分页查询
     */
    @RequestMapping("/pageList")
    @ResponseBody
    public Map<String, Object> pageList(@RequestParam(required = false, defaultValue = "0") int offset,
                                        @RequestParam(required = false, defaultValue = "10") int pagesize) {
        return bookService.pageList(offset, pagesize);
    }

}

