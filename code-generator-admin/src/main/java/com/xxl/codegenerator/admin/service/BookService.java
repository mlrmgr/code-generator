package com.xxl.codegenerator.admin.service;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.entity.Book;
import com.xxl.codegenerator.admin.model.ReturnT;

import java.util.Map;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:32:41'.
 */
public interface BookService {

    /**
     * 新增
     */
    public ReturnT<String> insert(Book book);

    /**
     * 删除
     */
    public ReturnT<String> delete(int id);

    /**
     * 更新
     */
    public ReturnT<String> update(Book book);

    /**
     * Load查询
     */
    public Book load(int id);

    /**
     * 分页查询
     */
    public Map<String,Object> pageList(int offset, int pagesize);

}

