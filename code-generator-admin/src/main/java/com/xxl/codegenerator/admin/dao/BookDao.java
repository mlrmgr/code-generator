package com.xxl.codegenerator.admin.dao;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/25/2019
 */
import com.xxl.codegenerator.admin.entity.Book;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 *
 *
 * Created by xuxueli on '2019-04-25 00:32:41'.
 */
@Component
public interface BookDao {

    /**
     * 新增
     */
    public int insert(@Param("book") Book book);

    /**
     * 删除
     */
    public int delete(@Param("id") int id);

    /**
     * 更新
     */
    public int update(@Param("book") Book book);

    /**
     * Load查询
     */
    public Book load(@Param("id") int id);

    /**
     * 分页查询Data
     */
    public List<Book> pageList(@Param("offset") int offset,
                               @Param("pagesize") int pagesize);

    /**
     * 分页查询Count
     */
    public int pageListCount(@Param("offset") int offset,
                             @Param("pagesize") int pagesize);

}

