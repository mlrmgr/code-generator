package com.xxl.codegenerator.admin.dao;
import com.xxl.codegenerator.admin.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 04/23/2019
 */

@Component
public interface UserDao {

    /**
     * 新增
     */
    public int insert(@Param("user") User user);

    /**
     * 删除
     */
    public int delete(@Param("id") int id);

    /**
     * 更新
     */
    public int update(@Param("user") User user);

    /**
     * Load查询
     */
    public User load(@Param("id") int id);

    /**
     * 分页查询Data
     */
    public List<User> pageList(@Param("offset") int offset,
                               @Param("pagesize") int pagesize);

    /**
     * 分页查询Count
     */
    public int pageListCount(@Param("offset") int offset,
                             @Param("pagesize") int pagesize);

}
